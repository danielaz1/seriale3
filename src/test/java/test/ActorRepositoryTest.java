package test;

import db.HsqldbConnection;
import db.RepositoryCatalog;
import db.repository.ActorRepository;
import domain.Actor;
import org.junit.Test;

import java.sql.Connection;
import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class ActorRepositoryTest {

	private final static String NAME_1 = "Name1";
	private final static String NAME_2 = "Name2";
	private final static String BIOGRAPHY_1 = "Biography1";
	private final static LocalDate DATE_1 = LocalDate.of(1990, 1, 1);

	@Test
	public void checkOperations() {
		Connection connection = new HsqldbConnection().getConnection();
		RepositoryCatalog repositoryCatalog = new RepositoryCatalog(connection);
		ActorRepository actorRepository = repositoryCatalog.actors();
		actorRepository.removeAll();
		Actor actor = new Actor(NAME_1, DATE_1, BIOGRAPHY_1);
		actorRepository.add(actor);
		ArrayList<Actor> actors = actorRepository.getAll();
		Actor actorRetrieved = actors.get(0);
		assertEquals(NAME_1, actorRetrieved.getName());
		assertEquals(BIOGRAPHY_1, actorRetrieved.getBiography());
		assertEquals(DATE_1, actorRetrieved.getDateOfBirth());

		actorRetrieved.setName(NAME_2);
		actorRepository.modify(actorRetrieved);
		actors = actorRepository.getAll();
		Actor actorRetrieved2 = actors.get(0);
		assertEquals(NAME_2, actorRetrieved2.getName());
		assertEquals(actorRetrieved.getId(), actorRetrieved2.getId());

		actorRepository.remove(actorRetrieved2);
		actors = actorRepository.getAll();
		assertEquals(0, actors.size());
	}

}
