package test;

import db.HsqldbConnection;
import db.RepositoryCatalog;
import db.repository.SeasonRepository;
import domain.Season;
import org.junit.Test;

import java.sql.Connection;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class SeasonRepositoryTest {

	private static final int YEAR_1 = 2010;
	private static final int NUMBER_1 = 1;
	private static final int YEAR_2 = 2011;
	private static final int NUMBER_2 = 2;

	@Test
	public void checkOperations() {
		Connection connection = new HsqldbConnection().getConnection();
		RepositoryCatalog repositoryCatalog = new RepositoryCatalog(connection);
		SeasonRepository seasonRepository = repositoryCatalog.seasons();
		seasonRepository.removeAll();
		Season season = new Season(NUMBER_1, YEAR_1);
		seasonRepository.add(season);

		ArrayList<Season> seasons = seasonRepository.getAll();
		Season seasonRetrieved = seasons.get(0);
		assertEquals(NUMBER_1, seasonRetrieved.getSeasonNumber());
		assertEquals(YEAR_1, seasonRetrieved.getYearOfRelease());

		seasonRetrieved.setSeasonNumber(NUMBER_2);
		seasonRetrieved.setYearOfRelease(YEAR_2);
		seasonRepository.modify(seasonRetrieved);
		Season seasonRetrieved2 = seasonRepository.withId(seasonRetrieved.getId());
		assertEquals(NUMBER_2, seasonRetrieved2.getSeasonNumber());
		assertEquals(YEAR_2, seasonRetrieved2.getYearOfRelease());

		seasonRepository.remove(seasonRetrieved2);
		seasons = seasonRepository.getAll();
		assertEquals(0, seasons.size());
	}

}
