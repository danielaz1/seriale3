package domain;

import java.time.LocalDate;

public abstract class Person {

	private long id;
	private String name;
	private LocalDate dateOfBirth;
	private String biography;

	public Person(String name, LocalDate dateOfBirth, String biography) {
		this.name = name;
		this.dateOfBirth = dateOfBirth;
		this.biography = biography;
	}

	public Person() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate date) {
		this.dateOfBirth = date;
	}

	public String getBiography() {
		return biography;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
