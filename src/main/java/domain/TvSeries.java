package domain;

import java.util.ArrayList;

public class TvSeries implements DomainObject {

	private long id;
	private String name;
	private ArrayList<Season> seasons;
	private ArrayList<Actor> actors;
	private Director director;

	public TvSeries(String name) {
		this.name = name;
	}

	public TvSeries() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;

	}

	public ArrayList<Season> getSeasons() {
		return seasons;
	}

	public void setSeasons(ArrayList<Season> seasons) {
		this.seasons = seasons;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ArrayList<Actor> getActors() {
		return actors;
	}

	public void setActors(ArrayList<Actor> actors) {
		this.actors = actors;
	}

	public Director getDirector() {
		return director;
	}

	public void setDirector(Director director) {
		this.director = director;
	}
}