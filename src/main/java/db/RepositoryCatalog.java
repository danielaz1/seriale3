package db;

import db.repository.*;

import java.sql.Connection;

public class RepositoryCatalog {

	private Connection connection;
	private ActorRepository actorRepository;
	private DirectorRepository directorRepository;
	private EpisodeRepository episodeRepository;
	private SeasonRepository seasonRepository;
	private TvSeriesRepository tvSeriesRepository;

	public RepositoryCatalog(Connection connection) {
		this.connection = connection;
		actorRepository = new ActorRepository(connection);
		directorRepository = new DirectorRepository(connection);
		episodeRepository = new EpisodeRepository(connection);
		seasonRepository = new SeasonRepository(connection);
		tvSeriesRepository = new TvSeriesRepository(connection);
	}

	public ActorRepository actors() {
		return actorRepository;
	}

	public DirectorRepository directors() {
		return directorRepository;
	}

	public EpisodeRepository episodes() {
		return episodeRepository;
	}

	public SeasonRepository seasons() {
		return seasonRepository;
	}

	public TvSeriesRepository tvSeries() {
		return tvSeriesRepository;
	}
}
