package db.mapper;

import domain.Actor;

import java.sql.*;

public class ActorMapper extends AbstractMapper<Actor> {

	private static final String FIND_BY_ID_SQL = "Select * from actor where id=?";
	private static final String FIND_ALL_SQL = "Select * from actor";
	private static final String INSERT_SQL = "INSERT INTO actor (name, date_of_birth, biography) VALUES (?,?,?)";
	private static final String UPDATE_SQL = "UPDATE actor SET name=?, date_of_birth=?, biography=? WHERE id=?";
	private static final String DELETE_SQL = "Delete from actor where id=?";
	private static final String SELECT_ID = "select id from actor";

	private TvSeriesActorMapper tvSeriesActorMapper;

	public ActorMapper(Connection connection) {
		super(connection);
		tvSeriesActorMapper = new TvSeriesActorMapper(connection);
	}

	@Override
	protected String findStatement() {
		return FIND_BY_ID_SQL;
	}

	@Override
	protected String getAllStatement() {
		return FIND_ALL_SQL;
	}

	@Override
	protected String insertStatement() {
		return INSERT_SQL;
	}

	@Override
	protected String updateStatement() {
		return UPDATE_SQL;
	}

	@Override
	protected String removeStatement() {
		return DELETE_SQL;
	}

	@Override
	protected String selectId() {
		return SELECT_ID;
	}

	@Override
	protected Actor doLoad(ResultSet rs) throws SQLException {
		Actor actor = new Actor();
		long actorId = rs.getLong("id");
		actor.setId(actorId);
		actor.setName(rs.getString("name"));
		actor.setDateOfBirth(rs.getDate("date_of_birth").toLocalDate());
		actor.setBiography(rs.getString("biography"));
		return actor;
	}

	@Override
	protected void parametrizeInsertStatement(PreparedStatement statement, Actor actor) throws SQLException {
		statement.setString(1, actor.getName());
		statement.setDate(2, Date.valueOf(actor.getDateOfBirth()));
		statement.setString(3, actor.getBiography());
	}

	@Override
	protected void parametrizeUpdateStatement(PreparedStatement statement, Actor actor) throws SQLException {
		parametrizeInsertStatement(statement, actor);
		statement.setLong(4, actor.getId());
	}

	@Override
	public void remove(long id) {
		try {
			tvSeriesActorMapper.removeByActorId(id);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		super.remove(id);
	}

}
