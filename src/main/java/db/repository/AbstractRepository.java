package db.repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class AbstractRepository<TEntity> implements Repository<TEntity> {

	protected Connection connection;

	public AbstractRepository(Connection connection) {
		this.connection = connection;
		try {
			if (! tableExists()) {
				Statement createTable = connection.createStatement();
				createTable.executeUpdate(getCreateTableSql());
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	private boolean tableExists() throws SQLException {
		ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
		while (rs.next()) {
			if (getTableName().equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
				return true;
			}
		}
		return false;
	}

	protected abstract String getTableName();

	protected abstract String getCreateTableSql();
}
